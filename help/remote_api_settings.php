<?php defined( 'ABSPATH' ) or die( 'No direct access allowed!' ); ?>

<p>Normally, the only thing that needs configuration here is the API key and email address.  To obtain an <strong>API key</strong>, please visit <a href="https://appreporter.oit.duke.edu">https://appreporter.oit.duke.edu</a>.</p>

<p>The production <strong>API endpoint</strong> for the API service is https://appreporter.oit.duke.edu/api.</p>

<p>Contact the <a href="mailto:security@duke.edu">Duke IT Security Office</a> if you need assistance.</p>
