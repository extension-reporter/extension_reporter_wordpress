#!/bin/bash

script_dir=$(cd $(dirname $0) && pwd -P)
new_version=$1

if [ -z "$new_version" ] ; then
  echo "Please specify a branch or tag to check out" > /dev/stderr
  exit 1
fi

build_dir=$(mktemp -d)
git clone git@gitlab.oit.duke.edu:extension-reporter/extension_reporter_library.git "$build_dir"
git -C "$build_dir" checkout "$new_version"
if [ $? -gt 0 ] ; then
  echo "Failed to check out $new_version"
  rm -rf "$build_dir"
  exit 1
fi

mkdir -p "$script_dir"/lib
if [ -d "$script_dir"/lib/extension_reporter_library ] ; then
  rm -rf "$script_dir"/lib/extension_reporter_library/
fi

rsync -r "$build_dir"/src/ "$script_dir"/lib/extension_reporter_library/
echo "$new_version" > "$script_dir"/lib/extension_reporter_library/version.txt

