<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 9/13/16
 * Time: 4:32 PM
 */

namespace DukeWebServices\ExtensionReporter\Entity;


class Extension {
  /**
   * @var string
   *   The name of the extension.
   */
  protected $name;

  /**
   * @var string
   *   The type of the extension, e.g. plugin, module, theme, etc.
   */
  protected $type;

  /**
   * @var string
   *   The version string for the installed version of the extension.
   */
  protected $versionInstalled;

  /**
   * @var boolean
   *   Flag indicating whether or not an update is available.
   */
  protected $updateAvailable;

  /**
   * @var boolean
   *   Flag indicating whether or not the extension is available.
   */
  protected $enabled;

  /**
   * @var boolean
   *   Flag indicating whether the installed version
   */
  protected $securityIssueVerified;

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return string
   */
  public function getVersionInstalled() {
    return $this->versionInstalled;
  }

  /**
   * @param string $versionInstalled
   */
  public function setVersionInstalled($versionInstalled) {
    $this->versionInstalled = $versionInstalled;
  }

  /**
   * @return boolean
   */
  public function isUpdateAvailable() {
    return $this->updateAvailable;
  }

  /**
   * @param boolean $updateAvailable
   */
  public function setUpdateAvailable($updateAvailable) {
    $this->updateAvailable = $updateAvailable;
  }

  /**
   * @return boolean
   */
  public function isEnabled() {
    return $this->enabled;
  }

  /**
   * @param boolean $enabled
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }

  /**
   * @return boolean
   */
  public function isSecurityIssueVerified() {
    return $this->securityIssueVerified;
  }

  /**
   * @param boolean $securityIssueVerified
   */
  public function setSecurityIssueVerified($securityIssueVerified) {
    $this->securityIssueVerified = $securityIssueVerified;
  }

}
