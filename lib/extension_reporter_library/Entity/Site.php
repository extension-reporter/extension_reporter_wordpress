<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 9/13/16
 * Time: 4:27 PM
 */

namespace DukeWebServices\ExtensionReporter\Entity;


class Site {
  /**
   * @var string
   *   The full name, or title of the site.
   */
  protected $name;

  /**
   * @var string
   *   The site domain.
   */
  protected $domain;

  /**
   * @var string
   *   The type of site.
   */
  protected $type;

  /**
   * @var string
   *   An email address for the site contact.
   */
  protected $contact;

  /**
   * @var Extension[]
   *   The extensions being reported for the site.
   */
  protected $extensions;

  public function __construct() {
    $this->extensions = array();
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getDomain() {
    return $this->domain;
  }

  /**
   * @param mixed $domain
   */
  public function setDomain($domain) {
    $this->domain = $domain;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param mixed $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return mixed
   */
  public function getContact() {
    return $this->contact;
  }

  /**
   * @param mixed $contact
   */
  public function setContact($contact) {
    $this->contact = $contact;
  }

  /**
   * @return Extension[]
   */
  public function getExtensions() {
    return $this->extensions;
  }

  /**
   * @param Extension $extension
   */
  public function addExtension(Extension $extension) {
    $this->extensions[] = $extension;
  }
}
