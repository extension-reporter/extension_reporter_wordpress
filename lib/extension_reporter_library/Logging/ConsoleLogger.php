<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 9/14/16
 * Time: 12:52 PM
 */

namespace DukeWebServices\ExtensionReporter\Logging;


class ConsoleLogger implements LoggerInterface {
  public function log($message, $severity) {
    switch ($severity) {
      case LoggerInterface::INFO:
        fputs(STDOUT, $message . "\n");
        break;

      case LoggerInterface::WARNING:
      case LoggerInterface::ERROR:
        fputs(STDERR, $message . "\n");
        break;
    }
  }
}
