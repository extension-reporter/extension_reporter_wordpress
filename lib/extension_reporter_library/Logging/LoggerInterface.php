<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 9/14/16
 * Time: 10:21 AM
 */

namespace DukeWebServices\ExtensionReporter\Logging;


interface LoggerInterface {
  const INFO = 1;
  const ERROR = 2;
  const WARNING = 3;

  public function log($message, $severity);
}
