<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 1:42 PM
 */

namespace DukeWebServices\ExtensionReporter\Reporter;


class ReporterResult {
  const E_NONE = 0;
  const E_COMMUNICAITON_FAILED = 1;
  const E_INVALID_RESPONSE = 2;

  /**
   * @var int
   */
  protected $errorNumber;

  /**
   * @var string
   */
  protected $errorMessage;

  /**
   * @var string
   */
  private $data;

  /**
   * ReporterResultBase constructor.
   */
  public function __construct() {
    $this->errorNumber = self::E_NONE;
    $this->errorMessage = '';
    $this->data = '';
  }

  /**
   * @return int
   */
  public function getErrorNumber() {
    return $this->errorNumber;
  }

  /**
   * @param int $errorNumber
   */
  public function setErrorNumber($errorNumber) {
    $this->errorNumber = $errorNumber;
  }

  /**
   * @return string
   */
  public function getErrorMessage() {
    return $this->errorMessage;
  }

  /**
   * @param string $errorMessage
   */
  public function setErrorMessage($errorMessage) {
    $this->errorMessage = $errorMessage;
  }

  /**
   * @return string
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @param string $data
   */
  public function setData($data) {
    $this->data = $data;
  }

  public function isSuccessful() {
    return $this->getErrorNumber() == self::E_NONE;
  }
}
