<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:32 AM
 */

namespace DukeWebServices\ExtensionReporter\Reporter;

use DukeWebServices\ExtensionReporter\Entity\Site;
use DukeWebServices\ExtensionReporter\Curl\CurlInterface;
use DukeWebServices\ExtensionReporter\Logging\LoggerInterface;

class CurlReporter implements ReporterInterface {
  /**
   * @var CurlInterface
   */
  protected $curl;

  /**
   * @var LoggerInterface
   */
  protected $logger;

  public function __construct(CurlInterface $curl = null, LoggerInterface $logger = null) {
    $this->curl = $curl;
    $this->logger = $logger;
  }

  /**
   * @return CurlInterface
   */
  public function getCurl() {
    return $this->curl;
  }

  /**
   * @param CurlInterface $curl
   */
  public function setCurl(CurlInterface $curl) {
    $this->curl = $curl;
  }

  /**
   * @return LoggerInterface
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * @param LoggerInterface $logger
   */
  public function setLogger(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * @inheritdoc
   */
  public function report(Site $site) {
    $transformer = new SiteArrayTransformer();
    $this->curl->setOption(CURLOPT_POST, 1);
    $this->curl->setOption(CURLOPT_POSTFIELDS, json_encode($transformer->transform($site)));
    $curlResponse = $this->curl->exec();
    $result = new ReporterResult();

    // check for error
    if ($curlResponse->getErrorNumber()) {
      $message = sprintf("Curl call to %s returned error with code %s: %s",
        $this->curl->getOption(CURLOPT_URL),
        $curlResponse->getErrorNumber(),
        $curlResponse->getErrorMessage()
      );
      $result->setErrorNumber($result::E_COMMUNICAITON_FAILED);
      $result->setErrorMessage($message);
      $this->log($message, LoggerInterface::ERROR);
      return $result;
    }

    // check for unexpected HTTP response code
    $info = $curlResponse->getInfo();
    if (!isset($info['http_code']) || $info['http_code'] != 201) {
      $message = sprintf("Curl call to %s returned HTTP code %s",
        $this->curl->getOption(CURLOPT_URL),
        isset($info['http_code']) ? $info['http_code'] : 'NULL'
      );
      $result->setErrorNumber($result::E_INVALID_RESPONSE);
      $result->setErrorMessage($message);
      $this->log($message, LoggerInterface::ERROR);
      return $result;
    }

    // log success message if we got this far.
    $message = sprintf('Curl call to %s was successful with data: %s',
      $this->getCurl()->getOption(CURLOPT_URL),
      $curlResponse->getData()
    );
    $this->log($message, LoggerInterface::INFO);

    return $result;
  }

  /**
   * Log a message, when a logger exists.
   *
   * @param $message
   * @param $severity
   */
  private function log($message, $severity) {
    if ($this->logger) {
      $this->logger->log($message, $severity);
    }
  }
}
