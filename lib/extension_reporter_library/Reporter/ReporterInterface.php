<?php

namespace DukeWebServices\ExtensionReporter\Reporter;

use DukeWebServices\ExtensionReporter\Entity\Site;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:31 AM
 */
interface ReporterInterface {
  /**
   * @param Site $site
   *   The site to report.
   *
   * @return bool
   *   Whether the report was successful.
   */
  public function report(Site $site);
}
