<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 12:00 PM
 */

namespace DukeWebServices\ExtensionReporter\Reporter;


use DukeWebServices\ExtensionReporter\Entity\Site;

class SiteArrayTransformer {
  /**
   * @param Site $site
   *
   * @return array
   */
  public function transform(Site $site) {
    $array = array(
      'site_name' => $site->getName(),
      'site_contact' => $site->getContact(),
      'site_domain' => $site->getDomain(),
      'site_type' => $site->getType(),
    );

    $array['extensions'] = array();
    foreach ($site->getExtensions() as $extension) {
      $array['extensions'][] = array(
        'name' => $extension->getName(),
        'type' => $extension->getType(),
        'version_installed' => $extension->getVersionInstalled() ? $extension->getVersionInstalled() : 'version_unspecified',
        'enabled' => $extension->isEnabled() ? 1 : 0,
        'update_available' => $extension->isUpdateAvailable() ? 1 : 0,
        'security_issue_verified' => $extension->isSecurityIssueVerified() ? 1 : 0,
      );
    }
    return $array;
  }
}
