<?php
/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 2:49 PM
 */

namespace DukeWebServices\ExtensionReporter\Reporter;


use DukeWebServices\ExtensionReporter\Curl\CurlDefault;
use DukeWebServices\ExtensionReporter\Curl\CurlInterface;
use DukeWebServices\ExtensionReporter\Logging\LoggerInterface;

class CurlReporterFactory {
  /**
   * @var string
   */
  protected $token;

  /**
   * @var string
   */
  protected $endpoint;

  /**
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * @var string
   */
  protected $curlClass;

  /**
   * @return string
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * @param string $token
   */
  public function setToken($token) {
    $this->token = $token;
  }

  /**
   * @return string
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * @param string $endpoint
   */
  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   * @return \DukeWebServices\ExtensionReporter\Logging\LoggerInterface
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * @param \DukeWebServices\ExtensionReporter\Logging\LoggerInterface $logger
   */
  public function setLogger($logger) {
    $this->logger = $logger;
  }

  /**
   * @return string
   */
  public function getCurlClass() {
    return $this->curlClass;
  }

  /**
   * @param string $curlClass
   */
  public function setCurlClass($curlClass) {
    $this->curlClass = $curlClass;
  }

  /**
   * @return CurlReporter
   */
  public function createReporter($path) {
    /** @var CurlInterface $curl */
    $curl = new $this->curlClass();
    $curl->setOption(CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Authorization: Token ' . $this->token,
    ));
    $curl->setOption(CURLOPT_URL, rtrim($this->endpoint, '/') . '/' . trim($path) . '/');
    $reporter = new CurlReporter($curl, $this->logger);
    return $reporter;
  }
}
