<?php

namespace DukeWebServices\ExtensionReporter\Curl;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:45 AM
 */
abstract class CurlBase implements CurlInterface {
  /**
   * @var array
   */
  protected $curlOptions;

  /**
   * CurlDefault constructor.
   */
  public function __construct() {
    $this->curlOptions = array();
    $this->setOption(CURLOPT_RETURNTRANSFER, 1);
    $this->setOption(CURLOPT_FOLLOWLOCATION, 1);
  }

  /**
   * @inheritdoc
   */
  public function setOption($name, $value) {
    $this->curlOptions[$name] = $value;
  }

  /**
   * @inheritdoc
   */
  public function getOption($name) {
    if (isset($this->curlOptions[$name])) {
      return $this->curlOptions[$name];
    }
    else {
      return null;
    }
  }
}
