<?php

namespace DukeWebServices\ExtensionReporter\Curl;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:55 AM
 */
class CurlDefault extends CurlBase {
  /**
   * @inheritdoc
   */
  public function exec() {
    $curl = curl_init();
    $response = new CurlResponse();

    curl_setopt_array($curl, $this->curlOptions);
    $response->setData(curl_exec($curl));
    if ($errno = curl_errno($curl)) {
      $response->setErrorNumber($errno);
      $response->setErrorMessage(curl_error($curl));
    }
    if ($info = curl_getinfo($curl)) {
      $response->setInfo($info);
    }
    return $response;
  }
}
