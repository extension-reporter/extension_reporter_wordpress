<?php

namespace DukeWebServices\ExtensionReporter\Curl;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:34 AM
 */
interface CurlInterface {

  /**
   * @param int $name
   *   The option to set, e.g. CURLOPT_URL.
   *
   * @param mixed $value
   *   The value of the option to set.
   *
   * @see curl_setopt()
   */
  public function setOption($name, $value);

  /**
   * @param int $name
   *   The option to get, e.g. CURLOPT_URL.
   *
   * @return mixed
   *   The value of the option.
   */
  public function getOption($name);

  /**
   * Execute the cURL request.
   *
   * @return CurlResponse
   *
   * @see curl_exec()
   */
  public function exec();

}
