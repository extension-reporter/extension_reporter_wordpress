<?php

namespace DukeWebServices\ExtensionReporter\Curl;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/26/16
 * Time: 11:49 AM
 */
class CurlResponse {
  /**
   * @var int
   */
  protected $errorNumber;

  /**
   * @var string
   */
  protected $errorMessage;

  /**
   * @var array
   */
  protected $info;

  /**
   * @var string
   */
  protected $data;

  public function __construct() {
    $this->errorNumber = CURLE_OK;
    $this->errorMessage = '';
    $this->info = array();
    $this->data = null;
  }

  /**
   * @return int
   */
  public function getErrorNumber() {
    return $this->errorNumber;
  }

  /**
   * @param int $errorNumber
   */
  public function setErrorNumber($errorNumber) {
    $this->errorNumber = $errorNumber;
  }

  /**
   * @return string
   */
  public function getErrorMessage() {
    return $this->errorMessage;
  }

  /**
   * @param string $errorMessage
   */
  public function setErrorMessage($errorMessage) {
    $this->errorMessage = $errorMessage;
  }

  /**
   * @return array
   */
  public function getInfo() {
    return $this->info;
  }

  /**
   * @param array $info
   */
  public function setInfo($info) {
    $this->info = $info;
  }

  /**
   * @return string
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @param string $data
   */
  public function setData($data) {
    $this->data = $data;
  }
}
