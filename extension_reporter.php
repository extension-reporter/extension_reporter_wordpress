<?php

defined( 'ABSPATH' ) or die( 'No direct access allowed!' );

define('EXTENSION_REPORTER_PLUGIN_ID', 'extension_reporter/extension_reporter.php');

/**
 * Plugin Name: Extension reporter
 * Plugin URI: http://webservices.duke.edu/drupalcode
 * Description: Provides logging about installed plugins and themes
 * Version: 2.0.7
 * Author: Duke Web Services
 * Author URI: http://webservices.duke.edu
 * Text Domain: Optional. Plugin's text domain for localization. Example: mytextdomain
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: true
 * License: GPL2
 */

if (is_admin()) {
  require_once __DIR__ .'/settings.php';
  $settingsPage = new ExtensionReporterSettingsPage();
  $settingsPage->setMultisite(is_multisite());
  $settingsPage->setTemplateDir(__DIR__ . '/templates');
  $settingsPage->setHelpDir(__DIR__ . '/help');
  $settingsPage->init();
}

add_action('activated_plugin', 'extension_reporter_activated_plugin');
add_action('deactivated_plugin', 'extension_reporter_deactivated_plugin');
add_action('extension_reporter_cron', 'extension_reporter_extension_reporter_cron');

/**
 * Implements hook activate_PLUGIN_NAME().
 */
function extension_reporter_activated_plugin($plugin, $network_wide = FALSE) {
  if ($plugin == EXTENSION_REPORTER_PLUGIN_ID) {
    wp_schedule_event(time(), 'hourly', 'extension_reporter_cron', array());
  }
}

/**
 * Implements hook uninstall_PLUGIN_NAME().
 */
function extension_reporter_deactivated_plugin($plugin, $network_deactivating = FALSE) {
  if ($plugin == EXTENSION_REPORTER_PLUGIN_ID) {
    wp_clear_scheduled_hook('extension_reporter_cron');
  }
}

/**
 * Log a status message.
 *
 * @param $message
 * @param string $type
 *   The type of message, i.e. 'success' or 'error'
 */
function extension_reporter_log($message, $type = 'success') {
  switch ($type) {
    case 'success':
      defined('WP_CLI') && WP_CLI::success($message);
      break;

    case 'error':
      defined('WP_CLI') && WP_CLI::error($message);
      error_log($message);
      break;
  }
}

/**
 * The extension_reporter_cron action.
 */
function extension_reporter_extension_reporter_cron() {
  require_once __DIR__ . '/settings.php';
  $options = new ExtensionReporterOptions(true);
  $result = extension_reporter_do_report($options->getValues(true));

  if ($result->isSuccessful()) {
    extension_reporter_log('Extension reporting was successful', 'success');
  }
  else {
    $message = sprintf('Extension reporting failed: [Code: %s] %s',
      $result->getErrorNumber(), $result->getErrorMessage());
    extension_reporter_log($message, 'error');
  }
}

/**
 * Implements hook extension_reporter_cron().
 */
function extension_reporter_do_report(array $options) {
  global $wp_version;

  // Register autoload function for library classes
  if (!in_array('extension_reporter_autoload', spl_autoload_functions())) {
    spl_autoload_register('extension_reporter_autoload');
  }

  $site = new \DukeWebServices\ExtensionReporter\Entity\Site();
  $site->setType('wordpress');
  $site->setContact($options['contact_email']);
  $site->setDomain(parse_url(get_option('siteurl'), PHP_URL_HOST));
  $site->setName(get_option('blogname'));

  // force version check, otherwise plugin update data may not be available in
  // some cases (e.g. after using wp-cli to update a single plugin)
  wp_version_check(array(), TRUE);


  $core_updates = get_core_updates();
  if (!empty($core_updates)) {
    $extension = new \DukeWebServices\ExtensionReporter\Entity\Extension();
    $extension->setType('core');
    $extension->setName('wordpress');
    $extension->setEnabled(true);
    $extension->setUpdateAvailable($core_updates[0]->response != 'latest');
    $extension->setVersionInstalled($wp_version);
    $extension->setSecurityIssueVerified($extension->isUpdateAvailable());
    $site->addExtension($extension);
  }

  $active_plugins = get_option('active_plugins');
  $plugin_updates = get_plugin_updates();
  foreach (get_plugins() as $plugin => $plugin_data) {
    $extension = new \DukeWebServices\ExtensionReporter\Entity\Extension();
    $extension->setType('plugin');
    $extension->setVersionInstalled(isset($plugin_data['Version']) ? $plugin_data['Version'] : 'not_reported');
    $extension->setName(preg_replace('#[\.\/].*#', '', $plugin));
    $extension->setEnabled(in_array($plugin, $active_plugins));
    $extension->setUpdateAvailable(isset($plugin_updates[$plugin]));
    $extension->setSecurityIssueVerified($extension->isUpdateAvailable());
    $site->addExtension($extension);
  }

  $theme_updates = get_theme_updates();
  foreach (wp_get_themes() as $theme => $theme_data) {
    $extension = new \DukeWebServices\ExtensionReporter\Entity\Extension();
    $extension->setType('theme');
    $extension->setVersionInstalled(isset($theme_data['Version']) ? $theme_data['Version'] : 'not_reported');
    $extension->setName($theme);
    $extension->setEnabled($theme_data->get_stylesheet_directory() == wp_get_theme()->get_stylesheet_directory());
    $extension->setUpdateAvailable(isset($theme_updates[$theme]));
    $extension->setSecurityIssueVerified($extension->isUpdateAvailable());
    $site->addExtension($extension);
  }

  $factory = new DukeWebServices\ExtensionReporter\Reporter\CurlReporterFactory();
  $factory->setEndpoint($options['remote_api_endpoint']);
  $factory->setToken($options['remote_api_key']);
  $factory->setCurlClass('DukeWebServices\\ExtensionReporter\\Curl\\CurlDefault');
  $reporter = $factory->createReporter('sites');

  // This option is dangerous and should only be used for testing, so it can
  // only be set via wp-cli
  if (get_option('extension_reporter_nossl_verification', 0)) {
    $reporter->getCurl()->setOption(CURLOPT_SSL_VERIFYHOST, 0);
    $reporter->getCurl()->setOption(CURLOPT_SSL_VERIFYPEER, 0);
  }

  return $reporter->report($site);
}

function extension_reporter_autoload($class) {
  if (preg_match('#^DukeWebServices\\\\ExtensionReporter\\\\(.*)#', $class, $matches)) {
    $filename = __DIR__ . '/lib/extension_reporter_library/' . str_replace('\\', '/', $matches[1]) . '.php';
    require_once $filename;
  }
  elseif (preg_match('#^WordPress\\\\ExtensionReporter\\\\(.*)#', $class, $matches)) {
    $filename = __DIR__ . '/src/' . str_replace('\\', '/', $matches[1]) . '.php';
    require_once $filename;
  }
}

