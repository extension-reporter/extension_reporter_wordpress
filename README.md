This plugin is a drop-and-go reporting tool that routes information about installed plugins and themes.

Values reported for the site:

- site_type: the type of site
- site_contact: the email address of the contact who is responsible for the site's security.
- site_domain: the domain of the site
- site_name: the name / title of the site as reported by site settings
- extensions: an array of extensions (modules and themes) For each extension, the following are reported:
  - name: the name of the extension, e.g. drupal, views, ctools.
  - type: the type of extension, i.e. core, module, theme
  - version_installed: The version that is currently installed
  - update_available: a boolean flag indicating whether the extension has an available update
  - security_issue_verified: a boolean flag that will be true if there is an available update that is marked as a security update
  - enabled: a boolean flag indicating whether or not the extension is enabled
  
# Run via Wp-cli

    wp cron event run extension_reporter_cron
