<?php

class ExtensionReporterOptions {
  const OPTION_NAME = 'extension_reporter_options';

  /**
   * @var array
   */
  private $options = array();

  /**
   * ExtensionReporterOptions constructor.
   */
  public function __construct($autoload = TRUE) {
    if ($autoload) {
      $this->load();
    }
  }

  /**
   * Provide default option values.
   *
   * @return array
   *   The option defaults, keyed by option name.
   */
  public function optionDefaults() {
    return array(
      'remote_api_endpoint' => array(
        'default_value' => '',
        'fallback' => 'https://appreporter.oit.duke.edu/api',
      ),
      'remote_api_key' => array(
        'default_value' => '',
        'fallback' => '',
      ),
      'contact_email' => array(
        'default_value' => '',
        'fallback' => get_option('admin_email', ''),
      ),
      'timestamp' => array(
        'default_value' => '',
        'fallback' => '',
      ),
    );
  }

  /**
   * Get values to use when none are provided.
   *
   * @return array
   *   The fallback option values.
   */
  public function getValues($withFallbacks = FALSE) {
    $values = array();
    foreach ($this->optionDefaults() as $name => $option_default) {
      if (!empty($this->options[$name])) {
        $values[$name] = $this->options[$name];
      }
      elseif ($withFallbacks) {
        $values[$name] = $option_default['fallback'];
      }
      else {
        $values[$name] = $option_default['default_value'];
      }
    }
    return $values;
  }

  /**
   * Set option values.
   *
   * @param array $values
   */
  public function setValues(array $values) {
    foreach (array_keys($this->optionDefaults()) as $name) {
      if (isset($values[$name])) {
        $this->options[$name] = $values[$name];
      }
    }
  }

  /**
   * Get fallback values.
   *
   * @return array
   *   The array of fallback values, keyed by option name.
   */
  public function getFallbackValues() {
    $values = array();
    foreach ($this->optionDefaults() as $name => $option_default) {
      $values[$name] = $option_default['fallback'];
    }
    return $values;
  }

  /**
   * Load options from the database.
   */
  public function load() {
    $option_defaults = $this->optionDefaults();

    if (empty($this->options)) {
      $this->options = get_option(self::OPTION_NAME, array());

      // set defaults for not-existing options.
      foreach ($option_defaults as $name => $option_default) {
        $this->options  += array(
          $name => $option_default['default_value'],
        );
      }

      // Remove any undefined options.
      foreach (array_keys($this->options) as $name) {
        if (!isset($option_defaults[$name])) {
          unset($this->options[$name]);
        }
      }
    }
  }

  /**
   * Save options to the database.
   */
  public function save() {
    $this->options['timestamp'] = time();
    if (get_option(self::OPTION_NAME)) {
      update_option(self::OPTION_NAME, $this->options);
    }
    else {
      add_option(self::OPTION_NAME, $this->options);
    }
  }


}

class ExtensionReporterSettingsPage {
  /**
   * @var ExtensionReporterOptions
   */
  private $options;

  /**
   * @var bool
   */
  protected $multisite = false;

  /**
   * @var string
   */
  protected $templateDir;

  /**
   * @var string
   */
  protected $helpDir;

  /**
   * @var array
   */
  protected $messages = array();

  /**
   * @return bool
   */
  public function isMultisite() {
    return $this->multisite;
  }

  /**
   * @param bool $multisite
   */
  public function setMultisite($multisite) {
    $this->multisite = $multisite;
  }

  /**
   * @return string
   */
  public function getTemplateDir() {
    return $this->templateDir;
  }

  /**
   * @param string $templateDir
   */
  public function setTemplateDir($templateDir) {
    $this->templateDir = $templateDir;
  }

  /**
   * @return string
   */
  public function getHelpDir() {
    return $this->helpDir;
  }

  /**
   * @param string $helpDir
   */
  public function setHelpDir($helpDir) {
    $this->helpDir = $helpDir;
  }

  private function getMenuAction() {
    return $this->isMultisite()
      ? 'network_admin_menu'
      : 'admin_menu';
  }

  public function addMenuPage() {
    add_menu_page(
      'Extension reporter configuration',
      'Extension reporter',
      'manage_options',
      'extension_reporter_config',
      array($this, 'renderPage')
    );
  }

  /**
   * Initialize the page.
   */
  public function init() {
    $this->options = new ExtensionReporterOptions();
    add_action($this->getMenuAction(), array($this, 'addMenuPage'));
  }

  /**
   * Render the page.
   */
  public function renderPage() {
    if (isset($_POST[ExtensionReporterOptions::OPTION_NAME])) {
      $this->update();
    }

    $form_action = '';
    $nonce = wp_create_nonce(ExtensionReporterOptions::OPTION_NAME);
    $help_path = $this->helpDir . '/remote_api_settings.php';
    $option_fallbacks = $this->options->getFallbackValues();
    $messages = $this->messages;
    $options = $this->options->getValues();

    // Sanitize potential user data
    foreach (array_keys($options) as $name) {
      $options[$name] = filter_var($options[$name], FILTER_SANITIZE_STRING);
    }

    include $this->templateDir . '/settings_form.php';
  }

  /**
   * Process the form submission.
   */
  public function update() {
    $errors = FALSE;

    if (!wp_verify_nonce($_POST['nonce'], ExtensionReporterOptions::OPTION_NAME)) {
      $this->setMessage('The nonce provided could not be verified', 'error');
      return;
    }

    $options = $_POST[ExtensionReporterOptions::OPTION_NAME];


    if (!empty($options['remote_api_key'])) {
      if (!preg_match('#^[0-9a-f]{40}$#', $options['remote_api_key'])) {
        $this->setMessage('API key must be 40-characters and only contain 0-9, a-f', 'error');
        $errors = TRUE;
      }
    }
    else {
      $this->setMessage('An API key is required', 'error');
      $errors = TRUE;
    }

    if (!empty($options['remote_api_endpoint'])) {
      if (filter_var($options['remote_api_endpoint'], FILTER_VALIDATE_URL)) {
        if (!preg_match('#^https://#', $options['remote_api_endpoint'])) {
          $this->setMessage('Remote API must begin with https://');
          $errors = TRUE;
        }
      }
      else {
        $this->setMessage('Remote API endpoint must be a valid URL');
        $errors = TRUE;
      }
    }

    if (!empty($options['contact_email'])) {
      if (!filter_var($options['contact_email'], FILTER_VALIDATE_EMAIL)) {
        $this->setMessage('Value provided for contact email is not a valid email address');
        $errors = TRUE;
      }
    }

    if (!$errors) {
      $errors = !$this->reportExtensions($options);
    }

    if ($errors) {
      return;
    }

    $this->options->setValues($options);
    $this->options->save();
    $this->setMessage('The options have been saved');
  }

  /**
   * Set a message to be displayed.
   *
   * @param $message
   *   The message to display.
   *
   * @param string $type
   *   The type of message, i.e. success, error.
   */
  private function setMessage($message, $type = 'success') {
    $this->messages[] = array(
      'raw' => $message,
      'sanitized' => filter_var($message, FILTER_SANITIZE_STRING),
      'type' => $type,
    );
  }

  /**
   * Action callback invoked after options are updated.
   */
  public function reportExtensions(array $options) {
    $fallbacks = $this->options->getFallbackValues();
    foreach ($options as $name => $value) {
      if (empty($value)) {
        $options[$name] = $fallbacks[$name];
      }
    }

    $result = extension_reporter_do_report($options);
    if ($result->isSuccessful()) {
      $this->setMessage('Extensions were reported successfully', 'success');
      return TRUE;
    }
    $this->setMessage(sprintf('Extensions could not be reported: %s', $result->getErrorMessage()), 'error');
    return FALSE;
  }

}
