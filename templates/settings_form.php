<div class="wrap">
    <h1>Extension reporter settings</h1>

    <?php foreach ($messages as $message): ?>
        <div class="notice notice-<?php echo $message['type'] ?>">
            <p><?php echo $message['sanitized'] ?></p>
        </div>
    <?php endforeach ?>

    <?php include $help_path ?>

    <form method="POST" action="<?php echo $form_action ?>">
        <input type="hidden" name="nonce" value="<?php echo $nonce ?>" />
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="extension_reporter_options[remote_api_key]">Remote API key</label></th>
                    <td>
                        <input type="text"
                               name="extension_reporter_options[remote_api_key]"
                               value="<?php echo $options['remote_api_key'] ?>"
                               class="regular-text"
                               placeholder="Enter API key"
                        />
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="extension_reporter_options[contact_email]">Contact email</label>
                    </th>
                    <td>
                        <input type="text"
                               name="extension_reporter_options[contact_email]"
                               value="<?php echo $options['contact_email'] ?>"
                               class="regular-text"
                               placeholder="<?php echo $option_fallbacks['contact_email'] ?>"
                        />
                        <p class="description">The email address of the person who is responsible for this site's security. Leave empty to use the administrator contact.</p>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="extension_reporter_options[remote_api_endpoint]">Remote API endpoint</label></th>
                    <td>
                        <input type="text"
                               name="extension_reporter_options[remote_api_endpoint]"
                               value="<?php echo $options['remote_api_endpoint'] ?>"
                               class="regular-text"
                               placeholder="<?php echo $option_fallbacks['remote_api_endpoint'] ?>"
                        />
                        <p class="description">The location to report plugins and themes to.  Do not enter a custom value in this field unless asked to do so.</p>
                    </td>
                </tr>

            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes" />
        </p>
    </form>
</div>
